var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}

function sheet(n) {
  if (n == 0) {
    document.getElementById("daloysheet").style.display = "block";
    document.getElementById("matproc").style.display = "none";
    document.getElementById("sl0").style.fontWeight = "bolder";
    document.getElementById("sl1").style.fontWeight = "normal";
  } else if (n == 1) {
    document.getElementById("daloysheet").style.display = "none";
    document.getElementById("matproc").style.display = "block";
    document.getElementById("sl0").style.fontWeight = "normal";
    document.getElementById("sl1").style.fontWeight = "bolder";
  }
}

function switchLayout(n) {
  if (n == 0) {
    document.getElementById("sheets").style.display = "block";
    document.getElementById("sscomps").style.display = "none";
    document.getElementById("navitem0").style.color = "black";
    document.getElementById("navitem1").style.color = "gray";
    document.getElementById("navitem2").style.color = "gray";
    sheet(0);
  } else if (n == 1) {
    document.getElementById("sheets").style.display = "block";
    document.getElementById("sscomps").style.display = "none";
    document.getElementById("navitem0").style.color = "gray";
    document.getElementById("navitem1").style.color = "black";
    document.getElementById("navitem2").style.color = "gray";
    sheet(1);
  } else if (n == 2) {
    document.getElementById("sscomps").style.display = "block";
    document.getElementById("sheets").style.display = "none";
    document.getElementById("navitem0").style.color = "gray";
    document.getElementById("navitem1").style.color = "gray";
    document.getElementById("navitem2").style.color = "black";
    showSlides(slideIndex)
  }
}